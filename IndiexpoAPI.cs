using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;



public class IndiexpoAPI : MonoBehaviour
{

    [Header("Basic setup")]
    public static IndiexpoAPI ieAPI;

    private bool preventRequests; //Use this to stop unwanted requests 


    [Tooltip("Insert the game code")]
    [SerializeField]
    private const string GAMEKEY = "";

    [Tooltip("Insert user code")]
    [SerializeField]
    private string userToken = "";

    public Token tokenClass;

    [System.Serializable]
    public class Token
    {
        [Header("Token and expiry date")]
        public string access_token = "";
        public string expires_at = "";

        public static Token CreateFromJSON(string jsonString)
        {
            return JsonUtility.FromJson<Token>(jsonString);
        }

        public static string ToJSON(Token jsonString)
        {
            return JsonUtility.ToJson(jsonString);
        }

    }


    public Score scoreClass;
    [System.Serializable]
    public class Score
    {

        public string success = "";


        public static Score CreateFromJSON(string jsonString)
        {
            return JsonUtility.FromJson<Score>(jsonString);
        }


    }


    void Awake()
    {
        if (ieAPI != null)
        {
            GameObject.Destroy(ieAPI);
        }
        else
        {
            ieAPI = this;

        }




        DontDestroyOnLoad(this);
    }




    // Use this for initialization
    void Start()
    {
        if (!GAMEKEY.Equals(""))
        {
            LoadAllFields();
                       

            if (tokenClass.access_token.Equals("") || tokenClass.access_token == null)
            {

                StartCoroutine(GenerateToken());

            }


        }
        else
        {
            Debug.Log("There's no GAMEKEY, make sure to generate it from your game page and then edit the GAMEKEY constant");
        }

    }



    //Use this method if you are building for non-webGL target
    public void GetUserCodeFromWebPage()
    {
        
        Application.OpenURL("https://www.indiexpo.net/api/v1/authentication");
        
    }


    //deprecated method, don't use it
    IEnumerator GetUserCode()
    {

        var userCode_URL = "https://www.indiexpo.net/api/v1/authentication";



        WWW wwwUserCode = new WWW(userCode_URL);

        PreventActions = true;

        yield return wwwUserCode;

        preventRequests = false;



        if (!string.IsNullOrEmpty(wwwUserCode.error))
        {
            Debug.Log("Get user code: " + wwwUserCode.error);

        }


        if (!wwwUserCode.text.Equals(""))
        {

            UserToken = wwwUserCode.text;


        }



    }


    public void StartTokenGeneration()
    {

        StartCoroutine(GenerateToken());

    }

    IEnumerator GenerateToken()
    {

        if (!GAMEKEY.Equals("") && !UserToken.Equals(""))
        {

            string wwwToken_URL = "https://www.indiexpo.net/api/v1/authentication";


            WWWForm tokenFORM = new WWWForm();


            tokenFORM.AddField("game_key", GAMEKEY);
            tokenFORM.AddField("user_token", UserToken);


            Debug.Log("Game key: " + GAMEKEY + " | User token:" + UserToken);

            WWW tokenRemote = new WWW(wwwToken_URL, tokenFORM);

            PreventActions = true;

            yield return tokenRemote;

            PreventActions = false;

            if (!string.IsNullOrEmpty(tokenRemote.error))
            {
                print("Generated token: " + tokenRemote.error);
            }
            else
            {
                Debug.Log(tokenRemote.text);
                tokenClass = Token.CreateFromJSON(tokenRemote.text);

                StartCoroutine(SendStandaloneNetScore(13));


                StartCoroutine(SaveAllFields());

            }


        }


    }


    //Use this method to send score
    public void SendScoreFromStandaloneBuild(int s)
    {
        StartCoroutine(SendStandaloneNetScore(s));


    }

    IEnumerator SendStandaloneNetScore(int s)
    {

        if (!tokenClass.access_token.Equals(""))
        {

            string wwwScore_URL = "https://www.indiexpo.net/api/v1/score";


            WWWForm scoreFORM = new WWWForm();


            scoreFORM.AddField("token", tokenClass.access_token);
            scoreFORM.AddField("score", s);


            UnityWebRequest scoreRemote = UnityWebRequest.Post(wwwScore_URL, scoreFORM);

            PreventActions = true;

            yield return scoreRemote.Send();

            PreventActions = false;

            if (scoreRemote.isNetworkError || scoreRemote.isHttpError)
            {
                Debug.Log("Send NETWORKING score: " + scoreRemote.error);
            }
            else
            {

                string responseScore = System.Text.Encoding.UTF8.GetString(scoreRemote.downloadHandler.data);
                scoreClass = Score.CreateFromJSON(responseScore);


                Debug.Log(UserToken + " " + GAMEKEY);

                Debug.Log("Response: " + responseScore);


                if (scoreClass.success.Equals("true"))
                {

                    Debug.Log("Uploaded score: " + s);

                }

                else
                {

                    Debug.Log("Score not uploaded");

                }

            }

        }

    }


    IEnumerator SaveAllFields()
    {

        PreventActions = true;

        PlayerPrefs.SetString("userToken", userToken);
        PlayerPrefs.SetString("access_token", tokenClass.access_token);
        PlayerPrefs.SetString("expires_at", tokenClass.expires_at);
        PlayerPrefs.Save();



        PreventActions = false;

        yield return null;

    }

    public void SaveUserToken()
    {

        PlayerPrefs.SetString("userToken", userToken);


    }

    public void LoadAllFields()
    {

        userToken = PlayerPrefs.GetString("userToken");
        tokenClass.access_token = PlayerPrefs.GetString("access_token");
        tokenClass.expires_at = PlayerPrefs.GetString("expires_at");



    }



    public string GameToken
    {
        get
        {
            return GAMEKEY;
        }

    }

    public string UserToken
    {
        get
        {
            return userToken;
        }

        set
        {
            userToken = value;
        }
    }

    public bool PreventActions
    {
        get
        {
            return preventRequests;
        }

        set
        {
            preventRequests = value;
        }
    }



}